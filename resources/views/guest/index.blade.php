@extends('layout.guest')
@section('titulo',"UnameCTF - Bem vindo")
@section('conteudo')
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <header>
                    <h2><i class="fa fa-newspaper-o" aria-hidden="true"></i> Painel de Notícias <small>Bem vindo Convidado</small></h2>
                </header>
                <section class="row">
                    <p>Mussum Ipsum, cacilds vidis litro abertis. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis. Admodum accumsan disputationi eu sit. Vide electram sadipscing et per. Detraxit consequat et quo num tendi nada. Cevadis im ampola pa arma uma pindureta.
                        Sapien in monti palavris qui num significa nadis i pareci latim. Delegadis gente finis, bibendum egestas augue arcu ut est. Pra lá , depois divoltis porris, paradis. Praesent vel viverra nisi. Mauris aliquet nunc non turpis scelerisque, eget.
                        Per aumento de cachacis, eu reclamis. Paisis, filhis, espiritis santis. Nullam volutpat risus nec leo commodo, ut interdum diam laoreet. Sed non consequat odio. Si num tem leite então bota uma pinga aí cumpadi!
                        Manduma pindureta quium dia nois paga. Quem num gosta di mim que vai caçá sua turmis! Si u mundo tá muito paradis? Toma um mé que o mundo vai girarzis! Vehicula non. Ut sed ex eros. Vivamus sit amet nibh non tellus tristique interdum. </p>
                </section>
            </div>
        </div>
    </div>
@endsection
