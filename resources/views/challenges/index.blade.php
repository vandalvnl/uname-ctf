<div class="row">
    <div class="container">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
            <div class="alert desafio-box">
                <h3>$getDesafio->nome</h3>
                <p>
                    $getDesafio->descricao $getDesafio->descricao bla bla bla bla
                    bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla
                </p>
                <p class="desafio-informacao">
                    <a data-toggle="modal" data-target="#desafio1">
                        <i class="fa fa-eye" aria-hidden="true"></i> Mais informações...
                    </a>
                </p>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
            <div class="alert desafio-box">
                <h3>$getDesafio->nome</h3>
                <p>
                    $getDesafio->descricao $getDesafio->descricao bla bla bla bla
                    bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla
                </p>
                <p class="desafio-informacao">
                    <a data-toggle="modal" data-target="#desafio2">
                        <i class="fa fa-eye" aria-hidden="true"></i> Mais informações...
                    </a>
                </p>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
            <div class="alert desafio-box">
                <h3>$getDesafio->nome</h3>
                <p>
                    $getDesafio->descricao $getDesafio->descricao bla bla bla bla
                    bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla
                </p>
                <p class="desafio-informacao">
                    <a data-toggle="modal" data-target="#desafio3">
                        <i class="fa fa-eye" aria-hidden="true"></i> Mais informações...
                    </a>
                </p>
            </div>
        </div>

    </div>
</div>